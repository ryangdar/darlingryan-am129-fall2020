import os
import sys
import numpy as np
import matplotlib.pyplot as plt 


def make_make():
    
    doesExecutableExist = os.path.exists('../PDE/advection_diffusion.exe')
    if not doesExecutableExist:
        os.chdir('../PDE')
        makeIt = 'make'
        os.system(makeIt)
        os.system('rm pde.init*')
    else:
        os.chdir('../PDE')
        makeCleanMake = 'make clean && make'
        os.system(makeCleanMake)
        os.system('rm pde.init*')
    
    
def runtimeParameters_init(Equation, Scheme, GridSize, InitLow, InitHigh, threshold, Advec, DiffConst, ClfConst, Uplim):
    
    os.chdir('../PDE')
    if os.path.exists('./pde.init'):
        commandLine = os.popen('ls ./pde.init* | wc -l')
        numberOfInitFiles = commandLine.readline()
        commandLine.close()
        
        newFileName = 'pde.init.'+str(numberOfInitFiles).lstrip()
        renamingFile = 'mv ./pde.init ./'+newFileName
        os.system(renamingFile)

        f=open('pde.init', 'w')
        f.write('simulation_type       '+Equation+"     # [char] Specify advection, diffusion, or advection_diffusion\n")
        f.write('discretization_type       '+Scheme+"     # [char] Specify 'center' or 'upwind'\n")
        f.write('grid_size    '+str(GridSize)+"     # [int] Choose number of elements in spatial domain'\n")
        f.write('x_min          '+str(InitLow)+"     # [real] minimum value of domain\n")
        f.write('x_max          '+str(InitHigh)+"     # [real] maximum value of domain\n")
        f.write('threshold      '+str(threshold)+"     # [real] Threshold value for solution convergence\n")
        f.write('advection_speed      '+str(Advec)+"     # [real]  Speed of traveling wave\n")
        f.write('diffusion_constant     '+str(DiffConst)+"     # [real] the diffusion constant\n")
        f.write('cfl_constant   '+str(ClfConst)+"        # [real]  Constant for CFL condition\n")
        f.write('max_time       '+str(Uplim)+"       # [real] maximum time, may be more than it takes to reach steady state")
        f.close()
        
    else:
        f=open('pde.init', 'w')
        f.write('simulation_type       '+Equation+"     # [char] Specify advection, diffusion, or advection_diffusion\n")
        f.write('discretization_type       '+Scheme+"     # [char] Specify 'center' or 'upwind'\n")
        f.write('grid_size    '+str(GridSize)+"     # [int] Choose number of elements in spatial domain'\n")
        f.write('x_min          '+str(InitLow)+"     # [real] minimum value of domain\n")
        f.write('x_max          '+str(InitHigh)+"     # [real] maximum value of domain\n")
        f.write('threshold      '+str(threshold)+"     # [real] Threshold value for solution convergence\n")
        f.write('advection_speed      '+str(Advec)+"     # [real]  Speed of traveling wave\n")
        f.write('diffusion_constant     '+str(DiffConst)+"     # [real] the diffusion constant\n")
        f.write('cfl_constant   '+str(ClfConst)+"        # [real]  Constant for CFL condition\n")
        f.write('max_time       '+str(Uplim)+"       # [real] maximum time, may be more than it takes to reach steady state")
        f.close()
    
    
def run_pde():
    os.chdir('../PDE')
    os.system('./advection_diffusion.exe')
      


""

def save_datFile(run_name):
    os.chdir('../PDE')
    if os.path.exists('./rootFinder_'+run_name.strip("'")+'.dat'):
        commandLine = os.popen('ls ./rootFinder_'+run_name+'.dat* | wc -l')
        numberOfDatFiles = commandLine.readline()
        commandLine.close()
    
        newFileName = 'rootFinder_'+run_name+'.dat.'+str(numberOfDatFiles).lstrip()
        renamingFile = 'mv ./rootFinder_'+run_name+'.dat ./'+newFileName
        os.system(renamingFile)



""

def plot_data(GridSize, Equation, Scheme, plotFilename, t):

    
    print(os.getcwd())
    os.chdir('../PDE')    

    print(plotFilename)
    
    figureFolder = './figures/'+Equation.strip("'")+'_'+str(GridSize)+'_'+Scheme.strip("'")
    if not os.path.exists( figureFolder ):
        os.system('mkdir '+figureFolder)

    
    figure_name = figureFolder+'/result_'+Equation.strip("'")+'_'+str(GridSize)+'_'+Scheme.strip("'")+'_'+str(t)+'.png'
    
    data = np.loadtxt('./results/'+plotFilename)
    fig = plt.figure()
    plt.plot(data[:,1],data[:,2],'o-')
    plt.ylabel('$u$')
    plt.grid(True)
    if (Equation == "'diffusion'"):
        plt.ylim(0,100)
        plt.title('Diffusion PDE')
    elif (Equation == "'advection'"):
        plt.title(Scheme.strip("'").title()+' Advection PDE, Time = '+str(t))
        plt.ylim(-1,1)
    else:
        plt.title(Scheme.strip("'").title()+' Advection-Diffusion PDE, Time = '+str(t))
        plt.ylim(-1,1)
        
    fig.savefig(figure_name)
    



def plot_data_g(GridSize, Equation, Scheme, plotFilename, t):

    
    print(os.getcwd())
    os.chdir('../PDE')    

    print(plotFilename)

    figure_name = './figures/plotsforh/result_'+Equation.strip("'")+'_'+str(GridSize)+'_'+Scheme.strip("'")+'_'+str(t)+'.png'
    
    data = np.loadtxt('./results/'+plotFilename)
    fig = plt.figure()
    plt.plot(data[:,1],data[:,2],'o-')
    plt.ylabel('$u$')
    plt.grid(True)
    plt.title(Scheme.strip("'").title()+' Advection PDE, Time = 1.0')
    plt.ylim(-1,1)
    plt.show()
    fig.savefig(figure_name)

   
   
def plot_data_h(GridSize, Equation, Scheme, plotFilename, t):

    print(os.getcwd())
    os.chdir('../PDE')    

    print(plotFilename)

    figure_name = './figures/plotsforh/result_'+Equation.strip("'")+'_'+str(GridSize)+'_'+Scheme.strip("'")+'_'+str(t)+'.png'

    data = np.loadtxt('./results/'+plotFilename)
    fig = plt.figure()
    plt.plot(data[:,1],data[:,2],'o-')
    plt.ylabel('$u$')
    plt.grid(True)
    plt.title(Scheme.strip("'").title()+' Advection-Diffusion PDE, Time = 1.0')
    plt.ylim(-1,1)
    plt.show()
    fig.savefig(figure_name)


if __name__ == '__main__':
    

    Equation     = "'advection_diffusion'"
    Scheme = "'center'"
    GridSize = 32
    InitLow = 0.0
    InitHigh = 1.0
    threshold = 1.e-4
    Advec = 1.0
    DiffConst = 1.156 #0.01
    ClfConst = 1.0
    Uplim = 1.0
    
    filenameList = []
    
    make_make()
    runtimeParameters_init(Equation, Scheme, GridSize, InitLow, InitHigh, threshold, Advec, DiffConst, ClfConst, Uplim)
    run_pde()
    
    for filename in os.listdir('../PDE/results'):
        filenameList.append(filename)


    print(filenameList)
    
    for i in range(0,len(filenameList)):
        plot_data_h(GridSize, Equation, Scheme, filenameList[i], i)

    
