!!------------------------------------------------------------------
!! A Fortran example code for finding a root of a user-defined 
!! function f(x) = 0.
!! 
!! This code is written by Prof. Dongwook Lee for AMS 209.
!!
!! This module has one subroutine which initialize your setup
!! by reading in runtime parameters from 'rootFinder.init' file.
!! The setup_init subroutine calls read_initFile*** subroutines
!! that are implemented as subroutines in read_initFile_module. 
!!
!!------------------------------------------------------------------

module setup_module

  use read_initFile_module
  
  implicit none

  integer, save :: GridSize
  real, save :: threshold
  real, save :: Advec
  real, save :: Uplim 
  real, save :: DiffConst
  real, save :: InitLow
  real, save :: InitHigh
  real, save :: ClfConst         
  real, save :: pi	
  character(len=80), save :: Equation 
  character(len=80), save :: Scheme
  
contains

  subroutine setup_init()

    implicit none
    
    pi = acos(-1.d0)
    
    call read_initFileInt('pde.init','GridSize', GridSize)
    call read_initFileReal('pde.init', 'threshold', threshold)
    call read_initFileReal('pde.init', 'Advec', Advec)
    call read_initFileReal('pde.init', 'Uplim', Uplim)
    call read_initFileReal('pde.init', 'DiffConst', DiffConst)
    call read_initFileReal('pde.init', 'InitLow', InitLow)
    call read_initFileReal('pde.init', 'InitHigh', InitHigh)
    call read_initFileReal('pde.init', 'ClfConst', ClfConst)
    call read_initFileChar('pde.init','Eq',Equation)
    call read_initFileChar('pde.init','Scheme', Scheme)
    
  end subroutine setup_init

  
end module setup_module
