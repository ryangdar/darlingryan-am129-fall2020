module pimod
    
    implicit none
    integer, parameter :: dp =kind(0.d0)
    real(dp), parameter :: pi_true = acos(-1.d0)
    
contains

    subroutine pi_approximation(thresh, N_Max, pi_appx, diff, N)
    
    implicit none
    real (dp), intent (in) :: thresh
    integer, intent(in) :: N_Max
    real (dp), intent (in out):: pi_appx
    real (dp), intent (in out):: diff
    integer, intent (in out):: N
    integer :: i
    
    pi_appx = 0.d0
    diff = abs(pi_appx-pi_true)
    N=0 
    
    do i = 0, N_Max
        pi_appx = pi_appx + (16.**(-i) * (4./(8*i+1.) -2./(8.*i+4.)-1./(8.*i+5.) -1./(8.*i+6.)))
        N = N+1
        diff = abs(pi_appx-pi_true)
        
        if (diff < thresh) then
            exit 
        end if 
    end do
    
    write(*,*) "N = ", N, " for threshold", thresh, "with diff", diff
    
    end subroutine pi_approximation 
    
end module pimod 