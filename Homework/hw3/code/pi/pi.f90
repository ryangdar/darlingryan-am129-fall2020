program pi
    use pimod
    implicit none 
    real (dp) :: pi_appx
    integer :: N
    real (dp) :: diff
    
    call pi_approximation(1.0d-4,50, pi_appx, diff, N)
    call pi_approximation(1.0d-8,50, pi_appx, diff, N)
    call pi_approximation(1.0d-12,50, pi_appx, diff, N)
    call pi_approximation(1.0d-16,50, pi_appx, diff, N)
end program pi 
        