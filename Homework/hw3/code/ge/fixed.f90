
subroutine gaussian_elimination(A,b)
    implicit none
    real, allocatable, dimension(:,:), intent(inout) :: A
    real, allocatable, dimension(:), intent(inout) :: b

    integer :: i,j
    real :: factor

    do j = 1, 2           
        do i = j+1, 3       
            factor = A(i,j)/A(j,j)
            A(i,:) = A(i,:) - factor*A(j,:)
            b(i) = b(i) - factor*b(j)
        end do
    end do

end subroutine gaussian_elimination

subroutine backsubstitution(A,b)
    implicit none
    real, allocatable, dimension(:,:), intent(inout) :: A
    real, allocatable, dimension(:), intent(inout) :: b
    integer :: i,j
    real :: factor

    do j = 3, 2, -1            
        do i = j-1, 1, -1        
            factor = A(i,j)/A(j,j)
            A(i,:) = A(i,:) - factor*A(j,:)
            b(i) = b(i) - factor*b(j)
        end do
    end do

    do i = 1, 3
        b(i) = b(i)/A(i,i)
        A(i,i) = A(i,i)/A(i,i)
    end do

end subroutine backsubstitution

program gauss
 INTERFACE 
    SUBROUTINE gaussian_elimination(A, b)
      real, allocatable, dimension(:,:), intent(inout) :: A
      real, allocatable, dimension(:), intent(inout) :: b
    END SUBROUTINE gaussian_elimination
 END INTERFACE
    INTERFACE 
    SUBROUTINE backsubstitution(A, b)
      real, allocatable, dimension(:,:), intent(inout) :: A
      real, allocatable, dimension(:), intent(inout) :: b
    END SUBROUTINE backsubstitution
 END INTERFACE

  real, allocatable, dimension(:,:) :: A
  real, allocatable, dimension(:) :: b
  integer :: i
  
 ALLOCATE(A(3, 3))
  A(:,:) = reshape((/2, 4, 7, 3, 7, 10, -1, 1, -4/), (/3,3/))
  b = (/1, 3, 4/)

   print *, ""    
   print *, "Inputted Matrix........"
  
  do i = 1, 3          
     print *, A(i,:), "|", b(i)
  end do

  print *, ""  
  print *, "Gaussian elimination........"
  call gaussian_elimination(A,b)
 
  print *, "***********************"
  do i = 1, 3
     print *, A(i,:), "|", b(i)
  end do

  print *, ""    
  print *, "back subs......"
  call backsubstitution(A,b)

  print *, "***********************"
  do i = 1, 3
     print *, A(i,:), "|", b(i)
  end do

  print *, ""
  print *, "The solution vector is;"
  do i = 1, 3
     print *, b(i)/A(i,i)
  end do

Deallocate(A)

end program gauss





