import os
import numpy as np
import matplotlib.pyplot as plt
import os, sys 
from pathlib import Path
import shutil

def rebuild():
    os.chdir("mathieu")
    os.system("make clean mathieu")
    path_parent = os.path.dirname(os.getcwd())
    os.chdir(path_parent)   

def build():
    os.chdir("mathieu")
    os.system("make mathieu")
    path_parent = os.path.dirname(os.getcwd())
    os.chdir(path_parent)  

def generate_input(N,q):
    my_file = Path("mathieu/mathieu.init")
    if my_file.is_file():
        shutil.copy('mathieu/mathieu.init','mathieu/mathieu.init.bak') 
    f = open('mathieu/mathieu.init', 'w+')
    file_content = f.read()
    f.write('num_points '+ str (N) + "\n" +  'q_index ' + str(q) + "\n" + 'run_name Mathieu_' + str (N) + '_' + str(q))
    f.close()

def run_mathieu(N,q):
    build()
    generate_input(N,q)
    os.chdir("mathieu") 
    os.system("./mathieu")
    path_parent = os.path.dirname(os.getcwd())
    os.chdir(path_parent)

def parsweep_mathieu(N):
    # Range of q values to test
    qRange = np.arange(0,42,2)
    for q in qRange:
        run_mathieu(N,q)

def plot_parsweep(N,nPlot):
    # Set range and space to store data
    qRange = np.arange(0,42,2)
    evals = np.zeros([len(qRange),nPlot])
    a = lambda q: 1+q-q**2/8-q**3/64
    b = lambda q: 4-q**2/12+5*q**4/13824
    # Open files and extract relevant data
    for i,q in enumerate(qRange):
        fname = 'mathieu/data/Mathieu_' + str(N) + '_' + str(q) + '.dat'
        data = np.loadtxt(fname)
        evals[i,:] = np.sort(data[:,1])[0:nPlot]
    # Generate a plot of the eigenvalues as a function of the parameter q
    plt.figure(figsize=(12,8))
    plt.plot(qRange,evals,'-k')
    plt.plot(qRange, a(qRange), 'b--')
    plt.plot(qRange, b(qRange), 'b--')
    plt.ylim(np.amin(evals)-10,np.amax(evals)+10)
    plt.title('Mathieu program')
    plt.xlabel('x-values', fontsize=18)
    plt.ylabel('y-values', fontsize=16)   
    plt.grid()
    plt.show()

if __name__=="__main__":
    N = 101
    rebuild()
    parsweep_mathieu(N)
    plot_parsweep(N,7)